# JSON - JavaScript Object Notation 🚀

Formato de texto simple representado por una `clave` que contiene un `valor`. Es una alternativa más limpia al formato `XML`.  

###### Ejemplo Sencillo

```json
{
	"clave": "valor"
}
```

###### Características

- Las `claves` deben estar en formato `String`, es decir entre comillas dobles.
- No se permiten valores numéricos que antepongan ceros (001). 
- Si el `valor` se encuentra entre comillas dobles, será representado como tipo `String`.
- Si el `valor` no se encuentra entre comillas sobles, será representado como tipo númerico.
- Si el `valor` no es un número y no se encuentra entre comillas dobles, el JSON estará mal formado y podría resultar en errores inesperados dentro de nuestra aplicación.
- JSON es un formato ampliamente usado en distintos lenguajes de programación backend para mandar datos al frontend y poderlos procesar y mostrar al usuario.
- JSON permite anidar objetos como `valor` de una sola `clave`, lo cual brinda posibilidades infinitas para formar objetos.

###### Ejemplos

1. Objeto Simple

```json
{
    "nombre": "Marcelo",
    "apellido": "Gallardo",
    "edad": 26,
    "programador": true,
    "pasatiempos": ["Gaming","Code","Sleep", "Learn"]
}
```


2. Arreglo de Objetos - Listado de Frutas 🍓

```json
[
    {
        "nombre": "Manzana",
        "color": "Verde"
    },
    {
        "nombre": "Frutilla",
        "color": "Rojo"
    },
    {
        "nombre": "Platano",
        "color": "Amarillo"
    },
    {
        "nombre": "Pera",
        "color": "Verde"
    },
    {
        "nombre": "Arandano",
        "color": "Azul"
    } 
]
```









